window.addEventListener('load', function() {
	var imageList = document.querySelectorAll("#thumbnails img");
	
	for ( i = 0; i< imageList.length; i++) {
		imageList[i].addEventListener('click', thumbnailClicked);
		//alert(imageList[i].src);
	}
	
	var figureElement = document.getElementById("featured");
	
	figureElement.addEventListener('mouseout', figureMouseOut);
	figureElement.addEventListener('mouseover', figureMouseOver);
	
});

function thumbnailClicked(e) {
	var mainImage = document.querySelector("#featured img");
	
	var smallVersionSRC = e.target.src;
	var largeVersionSRC = smallVersionSRC.replace("small","medium");
	
	mainImage.src = largeVersionSRC;
	
	
	var figureCaption = document.querySelector("#featured figcaption");
	figureCaption.innerHTML = e.target.title;
}
function figureMouseOut(e) {
	var figureCaption = document.querySelector("#featured figcaption");
	figureCaption.style.opacity = "0";	
}

function figureMouseOver(e) {
	
var figureCaption = document.querySelector("#featured figcaption");
figureCaption.style.opacity = "0.5";
}